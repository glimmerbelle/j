const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')

   .sass('resources/sass/app.scss', 'public/css')
   .styles(
      [
         'public/css/animate.css',
         'public/css/bootstrap.min.css',
         'public/css/et-line-icons.css',
         'public/css/font-awesome.min.css',
         'public/css/themify-icons.css',
         'public/css/swiper.min.css',
         'public/css/justified-gallery.min.css',
         'public/css/magnific-popup.css',
         'public/revolution/css/settings.css',
         'public/revolution/css/layers.css',
         'public/revolution/css/navigation.css',
         'public/css/bootsnav.css',
         'public/css/style.css',
         'public/css/responsive.css'
      ],
      'public/css/cityield.css');
      