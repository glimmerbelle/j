<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- title -->
        <title>{{ Config('app.name', 'Cityield')}}</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
        <meta name="author" content="Rommel">
        
        <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
        <link rel="apple-touch-icon" href="{{ asset('images/apple-touch-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/apple-touch-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/apple-touch-icon-114x114.png') }}">
        
        <link rel="stylesheet" href="{{ asset('css/cityield.css') }}" />
        
        <!--[if IE]>
            <script src="js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- start header -->
        <!-- start header -->
        @include('partials._header')
        <!-- end header -->
        <!-- start parallax hero section -->
        @include('partials._hero')
        <!-- end parallax hero section -->
        <!-- start about agency section -->
        <section class="wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-11 col-xs-12 center-col text-center margin-six-bottom xs-margin-30px-bottom">
                        <div class="alt-font text-medium-gray margin-5px-bottom text-uppercase text-small">About Digital Agency</div>
                        <h6 class="font-weight-300 text-extra-dark-gray no-margin">We always stay with our <strong class="font-weight-400">clients and respect</strong> their business. We deliver 100% and provide instant response to help them succeed in constantly changing and <strong class="font-weight-400">challenging business</strong> world.</h6>
                    </div>
                </div>
                <div class="row">
                    <!-- start feature box item -->
                    <div class="col-md-4 col-sm-4 col-xs-12 xs-margin-five-bottom last-paragraph-no-margin wow fadeInUp">
                        <div class="feature-box">
                            <div class="content">
                                <i class="icon-browser text-medium-gray icon-large margin-25px-bottom sm-margin-15px-bottom"></i>
                                <div class="text-medium alt-font text-capitalize text-extra-dark-gray margin-10px-bottom sm-margin-5px-bottom">Live Website Builder</div>
                                <p class="width-85 margin-lr-auto sm-width-100">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>        
                        </div>
                    </div>
                    <!-- end feature box item -->
                    <!-- start feature box item -->
                    <div class="col-md-4 col-sm-4 col-xs-12 xs-margin-five-bottom last-paragraph-no-margin wow fadeInUp">
                        <div class="feature-box">
                            <div class="content">
                                <i class="icon-book-open text-medium-gray icon-large margin-25px-bottom sm-margin-15px-bottom"></i>
                                <div class="text-medium alt-font text-capitalize text-extra-dark-gray margin-10px-bottom sm-margin-5px-bottom">Live Content Creator</div>
                                <p class="width-85 margin-lr-auto sm-width-100">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>
                    <!-- end feature box item -->
                    <!-- start feature box item -->
                    <div class="col-md-4 col-sm-4 col-xs-12 xs-margin-five-bottom last-paragraph-no-margin wow fadeInUp">
                        <div class="feature-box">
                            <div class="content">
                                <i class="icon-wallet text-medium-gray icon-large margin-25px-bottom sm-margin-15px-bottom"></i>
                                <div class="text-medium alt-font text-capitalize text-extra-dark-gray margin-10px-bottom sm-margin-5px-bottom">Create Unique Demos</div>
                                <p class="width-85 margin-lr-auto sm-width-100">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>   
                        </div>
                    </div>
                    <!-- end feature box item -->
                </div>
            </div>            
        </section>
        <!-- end about agency section -->
        <!-- start parallax with feature box section -->
        <section class="parallax one-second-screen parallax-feature-box sm-height-auto" data-stellar-background-ratio="0.3" style="background-image:url('http://placehold.it/1920x1200');">
            <div class="opacity-medium bg-extra-dark-gray"></div>
            <div class="container position-relative">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-8 text-center center-col sm-margin-60px-bottom xs-margin-40px-bottom">
                        <a class="popup-youtube" href="https://www.youtube.com/watch?v=nrJtHemSPW4">
                            <img src="images/icon-play.png" class="width-130px" alt=""/></a>
                        <h4 class="alt-font text-white">We are delivering beautiful digital products for you</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="parallax-feature-box-bottom z-index-5 wow fadeInUp">
                        <!-- start features box item -->
                        <div class="col-md-3 col-sm-6 col-xs-12 sm-margin-four-bottom xs-margin-eight-bottom wow fadeInUp">
                            <div class="bg-white box-shadow-light text-center padding-twelve-all position-relative xs-padding-five-all">
                                <i class="icon-desktop icon-extra-medium text-deep-pink margin-25px-bottom margin-25px-top sm-margin-15px-bottom"></i>
                                <div class="alt-font text-extra-dark-gray margin-10px-bottom sm-margin-5px-bottom font-weight-600">Unique Design</div>
                                <p>Lorem Ipsum is simply text of the printing and typesetting industry. Lorem Ipsum has been standard dummy.</p>
                            </div>
                        </div>
                        <!-- end feature box item -->
                        <!-- start features box item -->
                        <div class="col-md-3 col-sm-6 col-xs-12 sm-margin-four-bottom xs-margin-eight-bottom wow fadeInUp" data-wow-delay="0.2s">
                            <div class="bg-white box-shadow-light text-center padding-twelve-all position-relative xs-padding-five-all">
                                <i class="icon-tools icon-extra-medium text-deep-pink margin-25px-bottom margin-25px-top sm-margin-15px-bottom"></i>
                                <div class="alt-font text-extra-dark-gray margin-10px-bottom sm-margin-5px-bottom font-weight-600">Different Layout</div>
                                <p>Lorem Ipsum is simply text of the printing and typesetting industry. Lorem Ipsum has been standard dummy.</p>
                            </div>
                        </div>
                        <!-- end feature box item -->
                        <!-- start features box item -->
                        <div class="col-md-3 col-sm-6 col-xs-12 xs-margin-eight-bottom wow fadeInUp" data-wow-delay="0.4s">
                            <div class="bg-white box-shadow-light text-center padding-twelve-all position-relative xs-padding-five-all">
                                <i class="icon-target icon-extra-medium text-deep-pink margin-25px-bottom margin-25px-top sm-margin-15px-bottom"></i>
                                <div class="alt-font text-extra-dark-gray margin-10px-bottom sm-margin-5px-bottom font-weight-600">Portfolio Styles</div>
                                <p>Lorem Ipsum is simply text of the printing and typesetting industry. Lorem Ipsum has been standard dummy.</p>
                            </div>
                        </div>
                        <!-- end feature box item -->
                        <!-- start features box item -->
                        <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
                            <div class="bg-white box-shadow-light text-center padding-twelve-all position-relative xs-padding-five-all">
                                <i class="icon-laptop icon-extra-medium text-deep-pink margin-25px-bottom margin-25px-top sm-margin-15px-bottom"></i>
                                <div class="alt-font text-extra-dark-gray margin-10px-bottom sm-margin-5px-bottom font-weight-600">WordPress Theme</div>
                                <p>Lorem Ipsum is simply text of the printing and typesetting industry. Lorem Ipsum has been standard dummy.</p>
                            </div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                </div>
            </div>
        </section>
        <!-- end parallax with feature box section -->
        <!-- start feature box -->
        <section class="wow fadeIn bg-light-gray wow fadeIn">
            <div class="container margin-six-top md-margin-ten-top sm-no-margin-top">  
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12 sm-text-center sm-margin-40px-bottom xs-margin-30px-bottom">
                        <h5 class="text-extra-dark-gray font-weight-600 alt-font no-margin">The world's most powerful wordpress website builder</h5>
                    </div>
                    <!-- end feature box item -->
                    <!-- start feature box item -->
                    <div class="col-md-4 col-sm-6 col-xs-12 sm-text-center xs-margin-30px-bottom wow fadeIn last-paragraph-no-margin" data-wow-delay="0.2s">
                        <div class="col-md-3 text-center xs-no-padding-lr">
                            <h2 class="text-light-gray alt-font letter-spacing-minus-3 no-margin-bottom sm-margin-10px-bottom">01</h2>
                        </div>
                        <div class="col-md-9 margin-5px-top sm-text-center xs-no-padding-lr">
                            <span class="alt-font text-medium text-extra-dark-gray margin-5px-bottom display-block">Modern Framework</span>
                            <p class="width-80 md-width-100">Lorem Ipsum is simply text the printing and typesetting standard industry.</p>
                        </div>
                    </div>
                    <!-- end feature box item -->
                    <!-- start feature box item -->
                    <div class="col-md-4 col-sm-6 col-xs-12 sm-text-center wow fadeIn last-paragraph-no-margin" data-wow-delay="0.4s">
                        <div class="col-md-3 text-center xs-no-padding-lr">
                            <h2 class="text-light-gray alt-font letter-spacing-minus-3 no-margin-bottom sm-margin-10px-bottom">02</h2>
                        </div>
                        <div class="col-md-9 margin-5px-top sm-text-center xs-no-padding-lr">
                            <span class="alt-font text-medium text-extra-dark-gray margin-5px-bottom display-block">Live Website Builder</span>
                            <p class="width-80 md-width-100">Lorem Ipsum is simply text the printing and typesetting standard industry.</p>
                        </div>
                    </div>
                    <!-- end feature box item -->
                </div>
            </div>
        </section>
        <!-- end feature box section -->
        <!-- start portfolio section -->
        <section class="wow fadeIn border-bottom border-color-extra-light-gray no-padding-bottom">
            <div class="container-fluid">
                <div class="row text-center">
                    <div class="col-md-12">
                        <p class="alt-font text-medium-gray margin-5px-bottom text-uppercase text-small">Selected latest Works</p>
                        <h5 class="text-uppercase alt-font text-extra-dark-gray margin-20px-bottom font-weight-700 sm-width-100 xs-width-100">New Projects</h5>
                        <span class="separator-line-horrizontal-medium-light2 bg-deep-pink display-table margin-auto width-100px"></span>
                    </div>
                </div>
                <div class="row"> 
                    <div class="filter-content overflow-hidden margin-100px-top sm-margin-70px-top xs-margin-50px-top xs-margin-15px-lr">
                        <ul class="portfolio-grid work-4col gutter-large hover-option6 lightbox-portfolio">
                            <li class="grid-sizer"></li>
                            <!-- start portfolio item -->
                            <li class="grid-item wow fadeInUp last-paragraph-no-margin">
                                <figure>
                                    <div class="portfolio-img bg-deep-pink position-relative text-center overflow-hidden">
                                        <img src="http://placehold.it/800x650" alt=""/>
                                        <div class="portfolio-icon">
                                            <a href="single-project-page-01.html"><i class="fas fa-link text-extra-dark-gray" aria-hidden="true"></i></a>
                                            <a class="gallery-link" title="Lightbox gallery image title..." href="http://placehold.it/800x650"><i class="fas fa-search text-extra-dark-gray" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <figcaption class="bg-white">
                                        <div class="portfolio-hover-main text-center">
                                            <div class="portfolio-hover-box vertical-align-middle">
                                                <div class="portfolio-hover-content position-relative">
                                                    <a href="single-project-page-01.html"><span class="line-height-normal font-weight-600 text-small alt-font margin-5px-bottom text-extra-dark-gray text-uppercase display-block">Herbal Beauty Salon</span></a>
                                                    <p class="text-medium-gray text-extra-small text-uppercase">Branding and Identity</p>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </li>
                            <!-- end portfolio item -->
                            <!-- start portfolio item -->
                            <li class="grid-item wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.2s">
                                <figure>
                                    <div class="portfolio-img bg-deep-pink position-relative text-center overflow-hidden">
                                        <img src="http://placehold.it/800x650" alt=""/>
                                        <div class="portfolio-icon">
                                            <a href="single-project-page-02.html"><i class="fas fa-link text-extra-dark-gray" aria-hidden="true"></i></a>
                                            <a class="gallery-link" title="Lightbox gallery image title..." href="http://placehold.it/800x650"><i class="fas fa-search text-extra-dark-gray" aria-hidden="true"></i></a
                                            ></div>
                                    </div>
                                    <figcaption class="bg-white">
                                        <div class="portfolio-hover-main text-center">
                                            <div class="portfolio-hover-box vertical-align-middle">
                                                <div class="portfolio-hover-content position-relative">
                                                    <a href="single-project-page-02.html"><span class="line-height-normal font-weight-600 text-small alt-font margin-5px-bottom text-extra-dark-gray text-uppercase display-block">Tailoring Interior</span></a>
                                                    <p class="text-medium-gray text-extra-small text-uppercase">Branding and Brochure</p>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </li>
                            <!-- end portfolio item -->
                            <!-- start portfolio item -->
                            <li class="grid-item wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.4s">
                                <figure>
                                    <div class="portfolio-img bg-deep-pink position-relative text-center overflow-hidden">
                                        <img src="http://placehold.it/800x650" alt=""/>
                                        <div class="portfolio-icon">
                                            <a href="single-project-page-03.html"><i class="fas fa-link text-extra-dark-gray" aria-hidden="true"></i></a>
                                            <a class="gallery-link" title="Lightbox gallery image title..." href="http://placehold.it/800x650"><i class="fas fa-search text-extra-dark-gray" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <figcaption class="bg-white">
                                        <div class="portfolio-hover-main text-center">
                                            <div class="portfolio-hover-box vertical-align-middle">
                                                <div class="portfolio-hover-content position-relative">
                                                    <a href="single-project-page-03.html"><span class="line-height-normal font-weight-600 text-small alt-font margin-5px-bottom text-extra-dark-gray text-uppercase display-block">Designblast Inc</span></a>
                                                    <p class="text-medium-gray text-extra-small text-uppercase">Web and Photography</p>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </li>
                            <!-- end portfolio item -->
                            <!-- start portfolio item -->
                            <li class="grid-item wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.6s">
                                <figure>
                                    <div class="portfolio-img bg-deep-pink position-relative text-center overflow-hidden">
                                        <img src="http://placehold.it/800x650" alt=""/>
                                        <div class="portfolio-icon">
                                            <a href="single-project-page-04.html"><i class="fas fa-link text-extra-dark-gray" aria-hidden="true"></i></a>
                                            <a class="gallery-link" title="Lightbox gallery image title..." href="http://placehold.it/800x650"><i class="fas fa-search text-extra-dark-gray" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <figcaption class="bg-white">
                                        <div class="portfolio-hover-main text-center">
                                            <div class="portfolio-hover-box vertical-align-middle">
                                                <div class="portfolio-hover-content position-relative">
                                                    <a href="single-project-page-04.html"><span class="line-height-normal font-weight-600 text-small alt-font margin-5px-bottom text-extra-dark-gray text-uppercase display-block">HardDot Stone</span></a>
                                                    <p class="text-medium-gray text-extra-small text-uppercase">Branding and Identity</p>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </li>
                            <!-- end portfolio item -->
                            <!-- start portfolio item -->
                            <li class="grid-item wow fadeInUp last-paragraph-no-margin">
                                <figure>
                                    <div class="portfolio-img bg-deep-pink position-relative text-center overflow-hidden">
                                        <img src="http://placehold.it/800x650" alt=""/>
                                        <div class="portfolio-icon">
                                            <a href="single-project-page-05.html"><i class="fas fa-link text-extra-dark-gray" aria-hidden="true"></i></a>
                                            <a class="gallery-link" title="Lightbox gallery image title..." href="http://placehold.it/800x650"><i class="fas fa-search text-extra-dark-gray" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <figcaption class="bg-white">
                                        <div class="portfolio-hover-main text-center">
                                            <div class="portfolio-hover-box vertical-align-middle">
                                                <div class="portfolio-hover-content position-relative">
                                                    <a href="single-project-page-05.html"><span class="line-height-normal font-weight-600 text-small alt-font margin-5px-bottom text-extra-dark-gray text-uppercase display-block">Crop Identity</span></a>
                                                    <p class="text-medium-gray text-extra-small text-uppercase">Branding and Brochure</p>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </li>
                            <!-- end portfolio item -->
                            <!-- start portfolio item -->
                            <li class="grid-item wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.2s">
                                <figure>
                                    <div class="portfolio-img bg-deep-pink position-relative text-center overflow-hidden">
                                        <img src="http://placehold.it/800x650" alt=""/>
                                        <div class="portfolio-icon">
                                            <a href="single-project-page-06.html"><i class="fas fa-link text-extra-dark-gray" aria-hidden="true"></i></a>
                                            <a class="gallery-link" title="Lightbox gallery image title..." href="http://placehold.it/800x650"><i class="fas fa-search text-extra-dark-gray" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <figcaption class="bg-white">
                                        <div class="portfolio-hover-main text-center">
                                            <div class="portfolio-hover-box vertical-align-middle">
                                                <div class="portfolio-hover-content position-relative">
                                                    <a href="single-project-page-06.html"><span class="line-height-normal font-weight-600 text-small alt-font margin-5px-bottom text-extra-dark-gray text-uppercase display-block">Violator Series</span></a>
                                                    <p class="text-medium-gray text-extra-small text-uppercase">Web and Photography</p>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </li>
                            <!-- end portfolio item -->
                            <!-- start portfolio item -->
                            <li class="grid-item wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.4s">
                                <figure>
                                    <div class="portfolio-img bg-deep-pink position-relative text-center overflow-hidden">
                                        <img src="http://placehold.it/800x650" alt=""/>
                                        <div class="portfolio-icon">
                                            <a href="single-project-page-07.html"><i class="fas fa-link text-extra-dark-gray" aria-hidden="true"></i></a>
                                            <a class="gallery-link" title="Lightbox gallery image title..." href="http://placehold.it/800x650"><i class="fas fa-search text-extra-dark-gray" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <figcaption class="bg-white">
                                        <div class="portfolio-hover-main text-center">
                                            <div class="portfolio-hover-box vertical-align-middle">
                                                <div class="portfolio-hover-content position-relative">
                                                    <a href="single-project-page-07.html"><span class="line-height-normal font-weight-600 text-small alt-font margin-5px-bottom text-extra-dark-gray text-uppercase display-block">Jeremy Dupont</span></a>
                                                    <p class="text-medium-gray text-extra-small text-uppercase">Branding and Identity</p>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </li>
                            <!-- end portfolio item -->
                            <!-- start portfolio item -->
                            <li class="grid-item wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.6s">
                                <figure>
                                    <div class="portfolio-img bg-deep-pink position-relative text-center overflow-hidden">
                                        <img src="http://placehold.it/800x650" alt=""/>
                                        <div class="portfolio-icon">
                                            <a href="single-project-page-08.html"><i class="fas fa-link text-extra-dark-gray" aria-hidden="true"></i></a>
                                            <a class="gallery-link" title="Lightbox gallery image title..." href="http://placehold.it/800x650"><i class="fas fa-search text-extra-dark-gray" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <figcaption class="bg-white">
                                        <div class="portfolio-hover-main text-center">
                                            <div class="portfolio-hover-box vertical-align-middle">
                                                <div class="portfolio-hover-content position-relative">
                                                    <a href="single-project-page-08.html"><span class="line-height-normal font-weight-600 text-small alt-font margin-5px-bottom text-extra-dark-gray text-uppercase display-block">Bill Gardner</span></a>
                                                    <p class="text-medium-gray text-extra-small text-uppercase">Web and Photography</p>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </li>
                            <!-- end portfolio item -->
                        </ul>
                    </div>
                </div>
            </div> 
        </section>
        <!-- end portfolio section -->
        <!-- start section -->
        <section class="wow fadeIn animated position-relative sm-no-padding-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 padding-eleven-right md-padding-ten-right sm-text-center sm-padding-15px-right sm-margin-50px-bottom xs-margin-40px-bottom wow fadeIn xs-text-center">
                        <h4 class="font-weight-600 alt-font text-extra-dark-gray">Working to build a better web design</h4>
                        <p class="text-large alt-font font-weight-300">We are committed to customers success from start to finish. Our input helps make their solutions stand out from the crowd.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. Lorem Ipsum has been the industry. Lorem Ipsum has been the industry's standard dummy text since. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                        <a href="team-classic.html" class="btn btn-small btn-dark-gray">Meet The Team</a>
                    </div>
                    <div class="right-image">
                        <img src="images/image-right.png" alt=""/>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->
        <!-- start testimonials section -->
        <section class="wow fadeIn animated bg-light-gray">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-12">
                        <p class="alt-font text-medium-gray margin-5px-bottom text-uppercase text-small">what people says</p>
                        <h5 class="text-uppercase alt-font text-extra-dark-gray margin-20px-bottom font-weight-700 sm-width-100 xs-width-100">Testimonials</h5>
                        <span class="separator-line-horrizontal-medium-light2 bg-deep-pink display-table margin-auto width-100px"></span>
                    </div>
                </div>
                <div class="row">
                    <div class=" margin-100px-top sm-margin-70px-top sm-width-60 sm-center-col xs-width-100 xs-margin-50px-top">
                        <div class="col-md-4 sm-margin-two-bottom wow fadeIn last-paragraph-no-margin testimonial-style3">
                            <div class="testimonial-content-box padding-twelve-all bg-white border-radius-6 box-shadow arrow-bottom sm-padding-eight-all">
                                I wanted to hire the best and after looking at several other companies, I knew Jacob was the perfect guy for the job. He is a true professional.
                            </div>
                            <!-- start testimonials item -->
                            <div class="testimonial-box padding-25px-all xs-padding-20px-all">
                                <div class="image-box width-20"><img src="http://placehold.it/149x149" class="border-radius-100" alt=""></div>
                                <div class="name-box padding-20px-left">
                                    <div class="alt-font font-weight-600 text-small text-uppercase text-extra-dark-gray">Shoko Mugikura</div>
                                    <p class="text-extra-small text-uppercase text-medium-gray">Graphic Designer</p>
                                </div>
                            </div>
                        </div>
                        <!-- end testimonials item -->
                        <!-- start testimonials item -->
                        <div class="col-md-4 sm-margin-two-bottom wow fadeIn last-paragraph-no-margin testimonial-style3" data-wow-delay="0.2s">
                            <div class="testimonial-content-box padding-twelve-all bg-white border-radius-6 box-shadow arrow-bottom sm-padding-eight-all">
                                This is an excellent company! I personally enjoyed the energy and the professional support the whole team gave to us into creating website.
                            </div>
                            <div class="testimonial-box padding-25px-all xs-padding-20px-all">
                                <div class="image-box width-20"><img src="http://placehold.it/149x149" class="border-radius-100" alt=""></div>
                                <div class="name-box padding-20px-left">
                                    <div class="alt-font font-weight-600 text-small text-uppercase text-extra-dark-gray">Alexander Harvard</div>
                                    <p class="text-extra-small text-uppercase text-medium-gray">Creative Director</p>
                                </div>
                            </div>
                        </div>
                        <!-- end testimonials item -->
                        <!-- start testimonials item -->
                        <div class="col-md-4 wow fadeIn last-paragraph-no-margin testimonial-style3" data-wow-delay="0.4s">
                            <div class="testimonial-content-box padding-twelve-all bg-white border-radius-6 box-shadow arrow-bottom sm-padding-eight-all">
                                Their team are easy to work with and helped me make amazing websites in a short amount of time. Thanks again guys for all your hard work.
                            </div>
                            <div class="testimonial-box padding-25px-all xs-padding-20px-all">
                                <div class="image-box width-20"><img src="http://placehold.it/149x149" class="border-radius-100" alt=""></div>
                                <div class="name-box padding-20px-left">
                                    <div class="alt-font font-weight-600 text-small text-uppercase text-extra-dark-gray">Herman Miller</div>
                                    <p class="text-extra-small text-uppercase text-medium-gray">Co Founder / CEO</p>
                                </div>
                            </div>
                        </div>
                        <!-- end testimonials item -->
                    </div>
                </div> 
            </div>
        </section>
        <!-- end testimonial section  -->
        <!-- start feature box section -->
        <section class="wow fadeIn animated">
            <div class="container">
                <div class="row">
                    <!-- start feature box item -->
                    <div class="col-md-3 col-sm-6 col-xs-12 sm-margin-seven-bottom xs-margin-40px-bottom wow fadeInRight last-paragraph-no-margin">
                        <div class="feature-box-6 position-relative">
                            <i class="icon-tools icon-extra-medium text-deep-pink"></i>
                            <div class="alt-font text-extra-dark-gray font-weight-600 line-height-20">Powerful Options</div>
                            <p class="line-height-20">Shortcode subtitle</p>
                        </div>
                    </div>  
                    <!-- end feature box item -->
                    <!-- start feature box item -->
                    <div class="col-md-3 col-sm-6 col-xs-12 sm-margin-seven-bottom xs-margin-40px-bottom wow fadeInRight last-paragraph-no-margin" data-wow-delay="0.2s">
                        <div class="feature-box-6 position-relative">
                            <i class="icon-heart icon-extra-medium text-deep-pink"></i>
                            <div class="alt-font text-extra-dark-gray font-weight-600 line-height-20">Made with Love</div>
                            <p class="line-height-20">Shortcode subtitle</p>
                        </div>
                    </div>  
                    <!-- end feature box item -->
                    <!-- start feature box item -->
                    <div class="col-md-3 col-sm-6 col-xs-12 xs-margin-40px-bottom wow fadeInRight last-paragraph-no-margin" data-wow-delay="0.4s">
                        <div class="feature-box-6 position-relative">
                            <i class="icon-layers icon-extra-medium text-deep-pink"></i>
                            <div class="alt-font text-extra-dark-gray font-weight-600 line-height-20">Visual Page Builder</div>
                            <p class="line-height-20">Shortcode subtitle</p>
                        </div>
                    </div>  
                    <!-- end feature box item -->
                    <!-- start feature box item -->
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInRight last-paragraph-no-margin" data-wow-delay="0.6s">
                        <div class="feature-box-6 position-relative">
                            <i class="icon-expand icon-extra-medium text-deep-pink"></i>
                            <div class="alt-font text-extra-dark-gray font-weight-600 line-height-20">User Experience</div>
                            <p class="line-height-20">Shortcode subtitle</p>
                        </div>
                    </div>  
                    <!-- end feature box item -->
                </div>
            </div>
        </section>
        <!-- end feature box section -->
        <!-- start section -->
        <section class="wow fadeIn no-padding main-slider mobile-height content-right-slider">
            <div class="container-fluid">
                <div class="row equalize sm-equalize-auto">
                    <div class="col-md-6 cover-background sm-height-500px xs-height-350px wow fadeIn" style="background-image:url('http://placehold.it/1200x854')"></div>
                    <div class="swiper-full-screen swiper-container col-md-6 no-padding-lr white-move bg-extra-dark-gray text-center xs-padding-20px-tb wow fadeIn">
                        <div class="swiper-wrapper">
                            <!-- start slider item -->
                            <div class="swiper-slide last-paragraph-no-margin">
                                <div class="padding-eighteen-all md-padding-eight-all sm-padding-thirteen-all xs-padding-15px-lr">
                                    <div class="margin-30px-bottom text-center position-relative">
                                        <span class="title-large alt-font font-weight-100 text-dark-gray opacity4">01</span>
                                        <p class="alt-font font-weight-600 text-deep-pink text-uppercase position-absolute left-0 right-0 top-35 sm-top-25 xs-top-12">Let’s make something beautiful</p>
                                    </div>
                                    <h4 class="alt-font text-white">Unlimited power and customization possibilities</h4>
                                    <p class="width-90 sm-width-95 xs-width-85 center-col">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. Lorem Ipsum has been the industry. Lorem Ipsum has been the industry's standard dummy text since. Lorem Ipsum is simply dummy text of the typesetting industry.</p>
                                    <a href="about-us-classic.html" class="btn btn-small btn-white margin-35px-top"> About company</a>
                                </div>
                            </div>
                            <!-- end slider item -->
                            <!-- start slider item -->
                            <div class="swiper-slide last-paragraph-no-margin">
                                <div class="padding-eighteen-all md-padding-eight-all sm-padding-thirteen-all xs-padding-15px-lr">
                                    <div class="margin-30px-bottom text-center position-relative">
                                        <span class="title-large alt-font font-weight-100 text-dark-gray opacity4">02</span>
                                        <p class="alt-font font-weight-600 text-deep-pink text-uppercase position-absolute left-0 right-0 top-35 sm-top-25 xs-top-12">We are digital media agency</p>
                                    </div>
                                    <h4 class="alt-font text-white">We are delivering beautiful digital products for you</h4>
                                    <p class="width-90 sm-width-95 xs-width-85 center-col">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. Lorem Ipsum has been the industry. Lorem Ipsum has been the industry's standard dummy text since. Lorem Ipsum is simply dummy text of the typesetting industry.</p>
                                    <a href="about-us-classic.html" class="btn btn-small btn-white margin-35px-top"> About company</a>
                                </div>
                            </div>
                            <!-- end slider item -->
                            <!-- start slider item -->
                            <div class="swiper-slide last-paragraph-no-margin">
                                <div class="padding-eighteen-all md-padding-eight-all sm-padding-thirteen-all xs-padding-15px-lr">
                                    <div class="margin-30px-bottom text-center position-relative">
                                        <span class="title-large alt-font font-weight-100 text-dark-gray opacity4">03</span>
                                        <p class="alt-font font-weight-600 text-deep-pink text-uppercase position-absolute left-0 right-0 top-35 sm-top-25 xs-top-12">We create designs and technology</p>
                                    </div>
                                    <h4 class="alt-font text-white">We provide high quality & cost effective services</h4>
                                    <p class="width-90 sm-width-95 xs-width-85 center-col">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. Lorem Ipsum has been the industry. Lorem Ipsum has been the industry's standard dummy text since. Lorem Ipsum is simply dummy text of the typesetting industry.</p>
                                    <a href="about-us-classic.html" class="btn btn-small btn-white margin-35px-top"> About company</a>
                                </div>
                            </div>
                            <!-- end slider item -->
                        </div>
                        <!-- start slider pagination -->
                        <div class="swiper-button-next slider-long-arrow-white"></div>
                        <div class="swiper-button-prev slider-long-arrow-white"></div>
                        <!-- end slider pagination -->
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->
        <!-- start team section -->
        <section class="wow fadeIn">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-12">
                        <p class="alt-font margin-5px-bottom text-uppercase text-medium-gray text-small">Teamwork builds trust</p>
                        <h5 class="text-uppercase alt-font text-extra-dark-gray margin-20px-bottom font-weight-700 sm-width-100 xs-width-100">Creative People</h5>
                        <span class="separator-line-horrizontal-medium-light2 bg-deep-pink display-table margin-auto width-100px"></span>
                    </div>
                </div>
                <div class="row margin-100px-top sm-margin-70px-top xs-margin-50px-top">
                    <!-- start team item -->
                    <div class="col-md-3 col-sm-6 col-xs-12 team-block text-left team-style-1 sm-margin-seven-bottom xs-margin-30px-bottom wow fadeInRight">
                        <figure>
                            <div class="team-image xs-width-100">
                                <img src="http://placehold.it/700x892" alt="">
                                <div class="overlay-content text-center">
                                    <div class="display-table height-100 width-100">
                                        <div class="vertical-align-bottom display-table-cell icon-social-small padding-twelve-all">
                                            <span class="text-white text-small display-inline-block no-margin">Lorem Ipsum is simply dummy text of the printing and typesetting industry dummy text.</span>
                                            <div class="separator-line-horrizontal-full bg-deep-pink margin-eleven-tb"></div>
                                            <a href="http://www.facebook.com" target="_blank" class="text-white"><i class="fab fa-facebook-f"></i></a>
                                            <a href="http://www.twitter.com" target="_blank" class="text-white"><i class="fab fa-twitter"></i></a>
                                            <a href="http://plus.google.com" target="_blank" class="text-white"><i class="fab fa-google-plus-g"></i></a>
                                            <a href="http://instagram.com" target="_blank" class="text-white"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="team-overlay bg-extra-dark-gray opacity8"></div>
                            </div>
                            <figcaption>
                                <div class="team-member-position margin-20px-top text-center">
                                    <div class="text-small font-weight-500 text-extra-dark-gray text-uppercase">Herman Miller</div>
                                    <div class="text-extra-small text-uppercase text-medium-gray">Co-Founder / Design</div>
                                </div>   
                            </figcaption>
                        </figure>
                    </div>
                    <!-- end team item -->
                    <!-- start team item -->
                    <div class="col-md-3 col-sm-6 col-xs-12 team-block text-left team-style-1 sm-margin-seven-bottom xs-margin-30px-bottom wow fadeInRight" data-wow-delay="0.2s">
                        <figure>
                            <div class="team-image xs-width-100">
                                <img src="http://placehold.it/700x892" alt="">
                                <div class="overlay-content text-center">
                                    <div class="display-table height-100 width-100">
                                        <div class="vertical-align-bottom display-table-cell icon-social-small padding-twelve-all">
                                            <span class="text-white text-small display-inline-block no-margin">Lorem Ipsum is simply dummy text of the printing and typesetting industry dummy text.</span>
                                            <div class="separator-line-horrizontal-full bg-deep-pink margin-eleven-tb"></div>
                                            <a href="http://www.facebook.com" target="_blank" class="text-white"><i class="fab fa-facebook-f"></i></a>
                                            <a href="http://www.twitter.com" target="_blank" class="text-white"><i class="fab fa-twitter"></i></a>
                                            <a href="http://plus.google.com" target="_blank" class="text-white"><i class="fab fa-google-plus-g"></i></a>
                                            <a href="http://instagram.com" target="_blank" class="text-white"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="team-overlay bg-extra-dark-gray opacity8"></div>
                            </div>
                            <figcaption>
                                <div class="team-member-position margin-20px-top text-center">
                                    <div class="text-small font-weight-500 text-extra-dark-gray text-uppercase">Bill Gardner</div>
                                    <div class="text-extra-small text-uppercase text-medium-gray">Co-Founder / Design</div>
                                </div>   
                            </figcaption>
                        </figure>
                    </div>
                    <!-- end team item -->
                    <!-- start team item -->
                    <div class="col-md-3 col-sm-6 col-xs-12 team-block text-left team-style-1 xs-margin-30px-bottom wow fadeInRight" data-wow-delay="0.4s">
                        <figure>
                            <div class="team-image xs-width-100">
                                <img src="http://placehold.it/700x892" alt="">
                                <div class="overlay-content text-center">
                                    <div class="display-table height-100 width-100">
                                        <div class="vertical-align-bottom display-table-cell icon-social-small padding-twelve-all">
                                            <span class="text-white text-small display-inline-block no-margin">Lorem Ipsum is simply dummy text of the printing and typesetting industry dummy text.</span>
                                            <div class="separator-line-horrizontal-full bg-deep-pink margin-eleven-tb"></div>
                                            <a href="http://www.facebook.com" target="_blank" class="text-white"><i class="fab fa-facebook-f"></i></a>
                                            <a href="http://www.twitter.com" target="_blank" class="text-white"><i class="fab fa-twitter"></i></a>
                                            <a href="http://plus.google.com" target="_blank" class="text-white"><i class="fab fa-google-plus-g"></i></a>
                                            <a href="http://instagram.com" target="_blank" class="text-white"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="team-overlay bg-extra-dark-gray opacity8"></div>
                            </div>
                            <figcaption>
                                <div class="team-member-position margin-20px-top text-center">
                                    <div class="text-small font-weight-500 text-extra-dark-gray text-uppercase">Jeremy Dupont</div>
                                    <div class="text-extra-small text-uppercase text-medium-gray">Creative Director</div>
                                </div>   
                            </figcaption>
                        </figure>
                    </div>
                    <!-- end team item -->
                    <!-- start team item -->
                    <div class="col-md-3 col-sm-6 col-xs-12 team-block text-left team-style-1 wow fadeInRight" data-wow-delay="0.6s">
                        <figure>
                            <div class="team-image xs-width-100">
                                <img src="http://placehold.it/700x892" alt="">
                                <div class="overlay-content text-center">
                                    <div class="display-table height-100 width-100">
                                        <div class="vertical-align-bottom display-table-cell icon-social-small padding-twelve-all">
                                            <span class="text-white text-small display-inline-block no-margin">Lorem Ipsum is simply dummy text of the printing and typesetting industry dummy text.</span>
                                            <div class="separator-line-horrizontal-full bg-deep-pink margin-eleven-tb"></div>
                                            <a href="http://www.facebook.com" class="text-white" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                            <a href="http://www.twitter.com" class="text-white" target="_blank"><i class="fab fa-twitter"></i></a>
                                            <a href="http://plus.google.com" class="text-white" target="_blank"><i class="fab fa-google-plus-g"></i></a>
                                            <a href="http://instagram.com" class="text-white" target="_blank"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="team-overlay bg-extra-dark-gray opacity8"></div>
                            </div>
                            <figcaption>
                                <div class="team-member-position margin-20px-top text-center">
                                    <div class="text-small font-weight-500 text-extra-dark-gray text-uppercase">Sara Smith</div>
                                    <div class="text-extra-small text-uppercase text-medium-gray">Creative Studio Head</div>
                                </div>   
                            </figcaption>
                        </figure>
                    </div>
                    <!-- end team item -->
                </div>
            </div> 
        </section>
        <!-- end team section -->
        <!-- start blog section -->
        <section id="blog" class="bg-light-gray wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center margin-100px-bottom sm-margin-70px-bottom xs-margin-50px-bottom">
                        <p class="alt-font margin-5px-bottom text-uppercase text-medium-gray text-small">Publish what you think</p>
                        <h5 class="text-uppercase alt-font text-extra-dark-gray margin-20px-bottom font-weight-700 sm-width-100 xs-width-100">Recent Blog</h5>
                        <span class="separator-line-horrizontal-medium-light2 bg-deep-pink display-table margin-auto width-100px"></span>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-12 no-padding xs-padding-15px-lr">
                        <ul class="blog-grid blog-3col gutter-large blog-post-style5">
                            <li class="grid-sizer"></li>
                            <!-- start blog post item -->
                            <li class="grid-item wow fadeInUp last-paragraph-no-margin">
                                <div class="blog-post bg-white">
                                    <div class="blog-post-images overflow-hidden">
                                        <a href="blog-post-layout-01.html">
                                            <img src="http://placehold.it/900x650" alt="">
                                        </a>
                                        <div class="blog-categories bg-white text-uppercase text-extra-small alt-font"><a href="blog-masonry.html">Graphic Design</a></div>
                                    </div>
                                    <div class="post-details inner-match-height padding-40px-all sm-padding-20px-all xs-padding-30px-tb">
                                        <div class="blog-hover-color"></div>
                                        <a href="blog-post-layout-01.html" class="alt-font post-title text-medium text-extra-dark-gray display-block md-width-100 margin-5px-bottom">I like the body. I like to design everything to do with the body.</a>
                                        <div class="author">
                                            <span class="text-medium-gray text-uppercase text-extra-small display-inline-block">by <a href="blog-masonry.html" class="text-medium-gray">Jay Benjamin</a>&nbsp;&nbsp;|&nbsp;&nbsp;20 April 2017</span>
                                        </div>
                                        <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum standard dummy...</p>
                                    </div>
                                </div>
                            </li>
                            <!-- end blog post item -->
                            <!-- start blog post item -->
                            <li class="grid-item wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.2s">
                                <!-- start blog item -->
                                <div class="blog-post bg-white">
                                    <div class="blog-post-images overflow-hidden">
                                        <a href="blog-post-layout-02.html">
                                            <img src="http://placehold.it/900x650" alt="">
                                        </a>
                                        <div class="blog-categories bg-white text-uppercase text-extra-small alt-font"><a href="blog-masonry.html">Advertisement</a></div>
                                    </div>
                                    <div class="post-details inner-match-height padding-40px-all sm-padding-20px-all xs-padding-30px-tb">
                                        <div class="blog-hover-color"></div>
                                        <a href="blog-post-layout-02.html" class="alt-font post-title text-medium text-extra-dark-gray display-block md-width-100 margin-5px-bottom">Recognizing the need is the primary condition for design.</a>
                                        <div class="author">
                                            <span class="text-medium-gray text-uppercase text-extra-small display-inline-block">by <a href="blog-masonry.html" class="text-medium-gray">Jay Benjamin</a>&nbsp;&nbsp;|&nbsp;&nbsp;20 April 2017</span>
                                        </div>
                                        <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum standard dummy...</p>
                                    </div>
                                </div>
                                <!-- end blog item -->
                            </li>
                            <!-- end blog post item -->
                            <!-- start blog post item -->
                            <li class="grid-item wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.4s">
                                <div class="blog-post bg-white">
                                    <div class="blog-post-images overflow-hidden">
                                        <a href="blog-post-layout-03.html">
                                            <img src="http://placehold.it/900x650" alt="">
                                        </a>
                                        <div class="blog-categories bg-white text-uppercase text-extra-small alt-font"><a href="blog-masonry.html">Branding</a></div>
                                    </div>
                                    <div class="post-details inner-match-height padding-40px-all sm-padding-20px-all xs-padding-30px-tb">
                                        <div class="blog-hover-color"></div>
                                        <a href="blog-post-layout-03.html" class="alt-font post-title text-medium text-extra-dark-gray display-block md-width-100 margin-5px-bottom">Styles come and go. Design is a language, not a style.</a>
                                        <div class="author">
                                            <span class="text-medium-gray text-uppercase text-extra-small display-inline-block">by <a href="blog-masonry.html" class="text-medium-gray">Jay Benjamin</a>&nbsp;&nbsp;|&nbsp;&nbsp;20 April 2017</span>
                                        </div>
                                        <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum standard dummy...</p>
                                    </div>
                                </div>
                            </li>
                            <!-- end blog post item -->
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- end blog section -->
        <!-- start footer --> 
        @include('partials._footer')
        <!-- end footer -->
        <!-- start scroll to top -->
        <a class="scroll-top-arrow" href="javascript:void(0);"><i class="ti-arrow-up"></i></a>
        <!-- end scroll to top -->

    <!-- javascript libraries -->
    <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/skrollr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/smooth-scroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.appear.js') }}"></script>
    <!-- menu navigation -->
    <script type="text/javascript" src="{{ asset('js/bootsnav.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.nav.js') }}"></script>
    <!-- animation -->
    <script type="text/javascript" src="{{ asset('js/wow.min.js') }}"></script>
    <!-- page scroll -->
    <script type="text/javascript" src="{{ asset('js/page-scroll.js') }}"></script>
    <!-- swiper carousel -->
    <script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>
    <!-- counter -->
    <script type="text/javascript" src="{{ asset('js/jquery.count-to.js') }}"></script>
    <!-- parallax -->
    <script type="text/javascript" src="{{ asset('js/jquery.stellar.js') }}"></script>
    <!-- magnific popup -->
    <script type="text/javascript" src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
    <!-- portfolio with shorting tab -->
    <script type="text/javascript" src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <!-- images loaded -->
    <script type="text/javascript" src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <!-- pull menu -->
    <script type="text/javascript" src="{{ asset('js/classie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/hamburger-menu.js') }}"></script>
    <!-- counter  -->
    <script type="text/javascript" src="{{ asset('js/counter.js') }}"></script>
    <!-- fit video  -->
    <script type="text/javascript" src="{{ asset('js/jquery.fitvids.js') }}"></script>
    <!-- equalize -->
    <script type="text/javascript" src="{{ asset('js/equalize.min.js') }}"></script>
    <!-- skill bars  -->
    <script type="text/javascript" src="{{ asset('js/skill.bars.jquery.js') }}"></script> 
    <!-- justified gallery  -->
    <script type="text/javascript" src="{{ asset('js/justified-gallery.min.js') }}"></script>
    <!--pie chart-->
    <script type="text/javascript" src="{{ asset('js/jquery.easypiechart.min.js') }}"></script>
    <!-- instagram -->
    <script type="text/javascript" src="{{ asset('js/instafeed.min.js') }}"></script>
    <!-- retina -->
    <script type="text/javascript" src="{{ asset('js/retina.min.js') }}"></script>
    <!-- revolution -->
    <script type="text/javascript" src="{{ asset('revolution/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
    <!-- revolution slider extensions (load below extensions JS files only on local file systems to make the slider work! The following part can be removed on server for on demand loading) -->
    <!--<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>-->
    <!-- setting -->
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
</body>
</html>