<header>
    <!-- start navigation -->
    @include('partials._nav')
    <!-- end navigation --> 
    <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
        <button class="close-button-menu side-menu-close" id="close-pushmenu"></button>
        <div class="display-table padding-twelve-all height-100 width-100 text-center">
            <div class="display-table-cell vertical-align-top padding-70px-top position-relative">
                <div class="row">
                    <div class="col-lg-12 margin-70px-bottom">
                        <img src="images/logo-black-big.png" alt="" />
                    </div>
                    <div class="col-lg-12 margin-70px-bottom">
                        <img src="images/sidebar-image1.png" alt="" />
                    </div>
                    <div class="col-lg-12">
                        <h5 class="alt-font text-extra-dark-gray"><span class="display-block font-weight-300 text-dark-gray">The world's most</span><strong>powerful website builder.</strong></h5>
                    </div>
                    <div class="col-lg-12">
                        <a href="javascript:void(0);" class="btn btn-deep-pink btn-small text-extra-small border-radius-4"><i class="fas fa-play-circle icon-very-small margin-5px-right no-margin-left" aria-hidden="true"></i>Purchase Now</a>
                    </div>
                    <div class="col-md-12 margin-100px-top text-center">
                        <div class="icon-social-medium margin-three-bottom">
                            <a href="https://www.facebook.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://dribbble.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fab fa-dribbble" aria-hidden="true"></i></a>
                            <a href="https://plus.google.com" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a>
                            <a href="https://www.tumblr.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fab fa-tumblr" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end navigation -->  
</header>
