<nav class="navbar navbar-default bootsnav navbar-fixed-top header-light bg-transparent white-link">
    <div class="container nav-header-container">
        <div class="row">
            <!-- start logo -->
            <div class="col-md-2 col-xs-5">
                <a href="/" title="Cityield" class="logo"><img src="images/logo.png" data-rjs="images/logo@2x.png" class="logo-dark" alt="Pofo"><img src="images/logo-white.png" data-rjs="images/logo-white@2x.png" alt="Pofo" class="logo-light default"></a>
            </div>
            <!-- end logo -->
            <div class="col-md-7 col-xs-2 width-auto pull-right accordion-menu xs-no-padding-right">
                <button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#navbar-collapse-toggle-1">
                    <span class="sr-only">toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-collapse collapse pull-right" id="navbar-collapse-toggle-1">
                    <ul id="accordion" class="nav navbar-nav navbar-left no-margin alt-font text-normal" data-in="fadeIn" data-out="fadeOut">
                        <!-- start menu item -->
                        <li><a href="#">Travel</a></li>
                        <li><a href="#">Cars</a></li>
                        <li><a href="#">Hotels</a></li>
                        <li><a href="#">Holidays</a></li>
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                                                                
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-xs-5 width-auto">
                <div class="header-searchbar">
                    <a href="#search-header" class="header-search-form"><i class="fas fa-search search-button"></i></a>
                    <!-- search input-->
                    <form id="search-header" method="post" action="search-result.html" name="search-header" class="mfp-hide search-form-result">
                        <div class="search-form position-relative">
                            <button type="submit" class="fas fa-search close-search search-button"></button>
                            <input type="text" name="search" class="search-input" placeholder="Enter your keywords..." autocomplete="off">
                        </div>
                    </form>
                </div>
                <div class="heder-menu-button sm-display-none">
                    <button class="navbar-toggle mobile-toggle right-menu-button" type="button" id="showRightPush">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</nav>